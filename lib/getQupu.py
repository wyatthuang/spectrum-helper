#encoding:utf-8

import requests as rq
import lib.crawlerBasic as cb
from lxml import etree

crawlerTool = cb.crawlerComponent()


def search_for_item(song):
    url = 'http://www.qupu123.com/Search?keys=' + song + '&cid=16'
    songs = []

    # get cookie value
    # get the cookie

    IP = crawlerTool.get_an_ip()
    IP = {list(IP.keys())[0].lower():list(IP.values())[0].lower()}
    headers = crawlerTool.get_a_header()

    # compose the header
    cookieValue = dict(
        rq.post('http://www.qupu123.com', proxies=IP,timeout=30,verify=False).cookies)['damon_token_2019']
    print(cookieValue)

    headers['Cookie'] = 'damon_token_2019=' + cookieValue

    page = rq.get(url=url,proxies=IP,
                  headers=headers,timeout=30,verify=False).content.decode('utf-8')

    # to xpath form
    page = etree.HTML(page)

    # find component

    # class:    /html/body/table/tbody/tr[276]/td[2]/span/text() == [民歌曲谱]
    # song:     /html/body/table/tbody/tr[1]/td[2]/a/text()
    # Page:     /html/body/table/tbody/tr[1]/td[2]/@href
    # Composer: /html/body/table/tbody/tr[1]/td[3]/text()
    # Singer:   /html/body/table/tbody/tr[1]/td[4]/text()
    # second:   /html/body/table/tbody/tr[2]/td[2]/a
    # no find:  /html/body/div[3]/strong/text() == '提示'

    itemNum = 1
    for i in range(1):
        inf = {
            'status': page.xpath('/html/body/div[3]/strong/text()'),
            'class': page.xpath('/html/body/table/tbody/tr[276]/td[2]/span/text()'),
            'song' : page.xpath('/html/body/table/tbody/tr[1]/td[2]/a/text()'),
            'page' : page.xpath('/html/body/table/tbody/tr[1]/td[2]/@href'),
            'composer': page.xpath('/html/body/table/tbody/tr[1]/td[3]/text()'),
            'singer' : page.xpath('/html/body/table/tbody/tr[1]/td[4]/text()'),
        }

        songs.append(inf)

        itemNum += 1

    return songs


print(search_for_item('河'))

