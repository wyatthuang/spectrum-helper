# Spectrum Helper

This is developed for my grandpa. Although he is 74 years old now, he still attend to the music school persistently. For most of times, he always meet problem in finding the song’ spectrum, and let me to help him. Therefore, I start this project to providing my GP and his classmate an easier way to finding spectrum 

## Composed by

It mainly composed by three part:

1. crawler
2. cachePool
3. weChatAPI

And what I think is when the user ask me in weChat, like: `search song:I want it darker`, after program processing, my weChat account will return a message that contain this song’s spectrum automatically. Also, to speed up searching speed, and with considering the song that the user search will have high proportion to be the same, hence, a cachePool also contained in this project.

## Crawler

Resources from [qu123](http://www.qupu123.com/), proxies from [crawlerBasic V2(using cache)](https://gitlab.com/snippets/1873717)

## Cache Pool

use `pickle` because the data quantity is not very large

